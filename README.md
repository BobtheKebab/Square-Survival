# Square Survival

- A browser game with a simple premise: **Don't touch the other squares**
- I loved browser games as a kid, so I wanted to try making my own

## Technologies Used

- HTML
- JavaScript
- CSS

## Usage

After cloning the repository, simply open "myGame.html" with a browser

## Credits

I used [this tutorial](https://www.w3schools.com/graphics/game_intro.asp) as the starting point for my project
